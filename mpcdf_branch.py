#!/usr/bin/python2
from __future__ import print_function

import mpcdf_common

import osc
import osc.conf
import osc.core
import osc.cmdln


def do_mpcdf_branch(self, subcmd, opts, *args):
    """${cmd_name}: Branch package from software to your home project

    This creates a branch of the package PACKAGE in the central
    'software' repository into your home:$USER project, and sets
    all required meta-data for the enabled repositories.

    Usage:
        osc mpcdf_branch PACKAGE

    ${cmd_option_list}
    """

    apiurl = self.get_api_url()
    target_project = "home:" + osc.conf.get_apiurl_usr(apiurl)

    if len(args) == 0:
        raise osc.oscerr.WrongArgs("Missing argument: PACKAGENAME")
    if len(args) > 1:
        raise osc.oscerr.WrongArgs("Too many arguments")

    package, = args
    mpcdf_common.sync_projects(apiurl, package=package, from_project="software",
                               to_projects=(target_project,), add_to_maintainers=False)
