#!/usr/bin/python2
from __future__ import print_function

from mpcdf_common import mpcdf_setup_repositories

import os
import osc
import osc.conf
import osc.core
import osc.cmdln


@osc.cmdln.option('-n', '--dry-run', action="store_true",
                  help="Do not actually run anything but output the resulting XML configuration")
@osc.cmdln.option('--parent', metavar="PARENT",
                  help="Setup the repositories to be based on the upstream project PARENT (e.g. for home: projects)")
@osc.cmdln.option('--distribution',
                  help="Base distribution, necessary argument unless set previously for this project")
@osc.cmdln.option('--disable-repo', metavar="REPO", action="append",
                  help="Temporarily disable all repositories containing REPO")
@osc.cmdln.option('--only-project', action="store_true", default=False,
                  help="Only change project metadata 'prj' and 'prjconf', leave individual packages unchanged")
@osc.cmdln.option('--remove-old', action="store_true", default=False,
                  help="Remove all obsolete repositories instead of only disabling builds for packages there")
@osc.cmdln.option('--microarchitecture', metavar="ARCH", nargs=1,
                  help="Configure project to use ARCH as microarchitecture")
@osc.cmdln.alias("mpcdf_setup_repos")
def do_mpcdf_setup_repositories(self, subcmd, opts, *args):
    """${cmd_name}: Create all repository combinations for an MPCDF project

    Given a list of compilers, MPI libraries, and possibly CUDA versions, this command
    creates repositories for all the resulting combinations

    Usage:
        osc ${cmd_name} [PROJECT]

    ${cmd_option_list}

    """

    if len(args) == 0:
        if osc.core.is_project_dir(os.curdir) or osc.core.is_package_dir(os.curdir):
            project = osc.core.store_read_project(os.curdir)
        else:
            raise osc.oscerr.WrongArgs('Specify PROJECT or run command in an osc checkout directory')

    elif len(args) == 1:
        project, = args
    else:
        raise osc.oscerr.WrongArgs("Too many arguments")

    mpcdf_setup_repositories(self.get_api_url(),
                             project, microarchitecture=opts.microarchitecture, distribution=opts.distribution,
                             parent=opts.parent, dry_run=opts.dry_run, filter_repos=opts.disable_repo,
                             only_project=opts.only_project, remove_old=opts.remove_old)
